module.exports = function(bookshelf) {
  return bookshelf.Model.prototype.saveChanges = function(options) {
    if (options == null) {
      options = {};
    }
    options.patch = true;
    return this.save(this.changed, options);
  };
};
